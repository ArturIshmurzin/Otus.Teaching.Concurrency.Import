﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using ServiceStack.Text;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>, string>
    {
        public List<Customer> Parse(string input)
        {
            return CsvSerializer.DeserializeFromString<List<Customer>>(input);
        }
    }
}
