﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser: IDataParser<List<Customer>, IEnumerable<XElement>>
    {
        public List<Customer> Parse(IEnumerable<XElement> xElements)
        {
            List<Customer> result = new();

            XmlSerializer serializer = new XmlSerializer(typeof(Customer));
            foreach (XElement element in xElements)
            {
                result.Add((Customer)serializer.Deserialize(element.CreateReader()));
            }

            return result;
        }
    }
}