using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly DbSet<Customer> _customerSet;

        public CustomerRepository(AppDbContext appDbContext)
        {
            _appDbContext= appDbContext;
            _customerSet = _appDbContext.Set<Customer>();
        }

        public void AddCustomers(IEnumerable<Customer> customers)
        {
            _customerSet.AddRange(customers);
        }

        public void AddCustomersWithRetry(IEnumerable<Customer> customers, int tryCount)
        {
            var exceptions = new List<Exception>();

            for (int i = 0; i < tryCount; i++)
            {
                try
                {
                    if (i > 0)
                    {
                        Thread.Sleep(1000);
                    }
                    AddCustomers(customers);
                    this.Save();
                    return;
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }
            throw new AggregateException(exceptions);
        }

        public void Save()
        {
            _appDbContext.SaveChanges();
        }
    }
}