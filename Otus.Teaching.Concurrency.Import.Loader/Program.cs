﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Npgsql;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Generators;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.FileGenerators;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var host = AppStartup();

            Settings appSettings = host.Services.GetRequiredService<IOptions<Settings>>().Value;

            var dBContext = host.Services.GetService<AppDbContext>();
            if (dBContext.Database.GetPendingMigrations().Count() != 0)
                dBContext.Database.Migrate();

            var dataGenerator = host.Services.GetRequiredService<IDataGenerator>();

            dataGenerator.GenerateFile(appSettings.FilePath, appSettings.GeneratorDataQuantity);

            var dataLoader = host.Services.GetRequiredService<IDataLoader>();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            dataLoader.LoadData();

            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.TotalSeconds);
            Console.ReadKey();
        }

        private static IConfiguration BuildConfig()
        {
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            return config;
        }

        private static IHost AppStartup()
        {
            var config = BuildConfig();

            var host = Host.CreateDefaultBuilder()
                        .ConfigureServices((context, services) =>
                        {
                            services.AddTransient<ICustomerProvider, RandomCustomerGenerator>()
                            .AddDbContext<AppDbContext>(contextLifetime: ServiceLifetime.Transient, optionsLifetime: ServiceLifetime.Transient)
                            .AddTransient<IDbConnection>((sp) => new NpgsqlConnection(config.GetConnectionString("PostgreCustomer")))
                            .AddTransient<IDataParser<List<Customer>, IEnumerable<XElement>>, XmlParser>()
                            .AddTransient<IDataParser<List<Customer>, string>, CsvParser>()
                            .AddTransient<ICustomerRepository, CustomerRepository>()
                            .Configure<Settings>(options => config.GetSection("LoaderSettings").Bind(options));

                            string generatorDataFileMode = config.GetValue<string>("LoaderSettings:GeneratorDataFileMode");
                            switch (generatorDataFileMode)
                            {
                                case "Internal":
                                    services.AddTransient<IDataGenerator, InternalDataGenerator>();
                                    break;
                                case "External":
                                    services.AddTransient<IDataGenerator, ExternalDataGenerator>();
                                    break;
                                default:
                                    throw new Exception($"Unrecognized GeneratorDataFileMode: {generatorDataFileMode}");
                            }

                            string filePath = config.GetValue<string>("LoaderSettings:FilePath");
                            DirectoryInfo directoryInfo = new DirectoryInfo(filePath);
                            switch (directoryInfo.Extension)
                            {
                                case ".xml":
                                    services.AddTransient<IFileGenerator, XmlGenerator>()
                                    .AddTransient<IDataLoader, XmlDataLoaderWithThreadPool>();
                                    break;
                                case ".csv":
                                    services.AddTransient<IFileGenerator, CSVGenerator>()
                                    .AddTransient<IDataLoader, CsvDataLoader>();
                                    break;
                                default:
                                    throw new Exception($"Формат файла не поддерживается: {directoryInfo.Extension}");
                            }
                        })
                        .Build();

            return host;
        }
    }
}