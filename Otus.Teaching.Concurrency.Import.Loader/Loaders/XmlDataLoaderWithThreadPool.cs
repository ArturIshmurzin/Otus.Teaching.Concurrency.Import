﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class XmlDataLoaderWithThreadPool : IDataLoader
    {
        private readonly Settings _loaderSettings;
        private readonly IDataParser<List<Customer>, IEnumerable<XElement>> _dataParser;
        IServiceProvider _serviceProvider;

        public XmlDataLoaderWithThreadPool(IOptions<Settings> settings, IDataParser<List<Customer>, IEnumerable<XElement>> dataParser, IServiceProvider serviceProvider)
        {
            _loaderSettings = settings.Value;
            _dataParser = dataParser;
            _serviceProvider = serviceProvider;
        }

        public void LoadData()
        {
            LoadData(_loaderSettings.FilePath);
        }

        private void LoadData(string path)
        {
            var document = XDocument.Load(path);
            var users = document.Descendants("Customer");

            var countdownEvent = new CountdownEvent(1);

            int batchSize = users.Count() / _loaderSettings.ThreadsQuantity;

            if (users.Count() % _loaderSettings.ThreadsQuantity != 0)
                batchSize++;

            int batchNumber = 0;

            while (batchNumber < _loaderSettings.ThreadsQuantity)
            {
                countdownEvent.AddCount(1);

                var usersBatch = users.Skip(batchNumber * batchSize).Take(batchSize);

                ThreadPool.QueueUserWorkItem(state =>
                {
                    var customers = _dataParser.Parse(usersBatch);
                    ICustomerRepository customerRepository = _serviceProvider.GetRequiredService<ICustomerRepository>();
                    customerRepository.AddCustomersWithRetry(customers, 3);
                    countdownEvent.Signal();
                });

                batchNumber++;
            }

            countdownEvent.Signal();
            countdownEvent.Wait();
        }
    }
}
