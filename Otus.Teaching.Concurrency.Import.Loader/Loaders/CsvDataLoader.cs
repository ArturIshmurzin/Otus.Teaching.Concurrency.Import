﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class CsvDataLoader : IDataLoader
    {
        private readonly Settings _settings;
        private readonly IDataParser<List<Customer>, string> _dataParser;
        IServiceProvider _serviceProvider;

        public CsvDataLoader(IOptions<Settings> settings, IDataParser<List<Customer>, string> dataParser,
            IServiceProvider serviceProvider)
        {
            _settings = settings.Value;
            _dataParser = dataParser;
            _serviceProvider = serviceProvider;
        }

        public void LoadData()
        {
            LoadData(_settings.FilePath);
        }

        private void LoadData(string path)
        {
            var lines = File.ReadAllLines(path);
            var columns = lines[0];
            lines = lines.Skip(1).ToArray();

            var countdownEvent = new CountdownEvent(1);

            int batchSize = lines.Length / _settings.ThreadsQuantity;

            if (lines.Length % _settings.ThreadsQuantity != 0)
                batchSize++;

            int batchNumber = 0;

            while (batchNumber < _settings.ThreadsQuantity)
            {
                countdownEvent.AddCount(1);

                var usersBatch = lines.Skip(batchNumber * batchSize).Take(batchSize);

                var thread = new Thread(() =>
                {
                    var customers = _dataParser.Parse($"{columns}{Environment.NewLine}{string.Join(Environment.NewLine, usersBatch)}");

                    ICustomerRepository customerRepository = _serviceProvider.GetRequiredService<ICustomerRepository>();
                    customerRepository.AddCustomersWithRetry(customers, 3);

                    countdownEvent.Signal();
                });
                thread.Start();

                batchNumber++;
            }

            countdownEvent.Signal();
            countdownEvent.Wait();
        }
    }
}
