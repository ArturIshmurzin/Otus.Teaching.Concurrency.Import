﻿namespace Otus.Teaching.Concurrency.Import.Core.Parsers
{
    public interface IDataParser<TResult, TInput>
    {
        TResult Parse(TInput input);
    }
}