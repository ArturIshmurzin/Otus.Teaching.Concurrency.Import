﻿namespace Otus.Teaching.Concurrency.Import.Core
{
    public class Settings
    {
        public string FilePath { get; set; }
        public int ThreadsQuantity { get; set; }
        public int GeneratorDataQuantity { get; set; }
        public string GeneratorDataFileProcessPath { get; set; }
        public string GeneratorDataFileMode { get; set; }
    }
}
