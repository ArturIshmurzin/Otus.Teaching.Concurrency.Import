using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Handler.Data
{
    public interface ICustomerProvider
    {
        List<Customer> Generate(int count);
    }
}