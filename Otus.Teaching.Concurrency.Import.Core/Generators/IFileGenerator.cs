﻿namespace Otus.Teaching.Concurrency.Import.Core.Generators
{
    public interface IFileGenerator
    {
        void GenerateFile(string path, int dataCount);
    }
}
