using System;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Generators;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class XmlGenerator : IFileGenerator
    {
        private readonly ICustomerProvider _dataGenerator;

        public XmlGenerator(ICustomerProvider dataGenerator)
        {
            _dataGenerator = dataGenerator ?? throw new ArgumentNullException(nameof(dataGenerator));
        }

        public void GenerateFile(string path, int dataCount)
        {
            var customers = _dataGenerator.Generate(dataCount);
            using var stream = File.Create(path);
            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Customers = customers
            });
        }
    }
}