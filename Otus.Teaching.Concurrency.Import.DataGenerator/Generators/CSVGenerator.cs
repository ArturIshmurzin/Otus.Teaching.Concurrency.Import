﻿using Otus.Teaching.Concurrency.Import.Core.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using ServiceStack.Text;
using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CSVGenerator : IFileGenerator
    {
        private readonly ICustomerProvider _dataGenerator;

        public CSVGenerator(ICustomerProvider dataGenerator)
        {
            _dataGenerator = dataGenerator ?? throw new ArgumentNullException(nameof(dataGenerator));
        }

        public void GenerateFile(string path, int dataCount)
        {
            var customers = _dataGenerator.Generate(dataCount);
            
            using (var stream = File.Create(path))
            {
                CsvSerializer.SerializeToStream(customers, stream);
            }

        }
    }
}
