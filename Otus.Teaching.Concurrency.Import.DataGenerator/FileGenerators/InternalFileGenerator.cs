﻿using Otus.Teaching.Concurrency.Import.Core.Generators;
using System;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.FileGenerators
{
    public class InternalDataGenerator : IDataGenerator
    {
        private readonly IFileGenerator _fileGenerator;
        public InternalDataGenerator(IFileGenerator fileGenerator)
        {
            _fileGenerator = fileGenerator ?? throw new ArgumentNullException(nameof(fileGenerator));
        }

        public void GenerateFile(string path, int dataCount)
        {
            _fileGenerator.GenerateFile(path, dataCount);
        }
    }
}
