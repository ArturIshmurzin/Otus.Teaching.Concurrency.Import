﻿using Microsoft.Extensions.Options;
using Microsoft.VisualBasic.FileIO;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Generators;
using System;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.FileGenerators
{
    public class ExternalDataGenerator : IDataGenerator
    {
        private readonly Settings _settings;
        public ExternalDataGenerator(IOptions<Settings> settings)
        {
            _settings = settings.Value;
        }

        public void GenerateFile(string path, int dataCount)
        {
            string arguments = $"\"{path}\" {dataCount}";

            Process process = new Process();
            process.StartInfo.FileName = _settings.GeneratorDataFileProcessPath;
            process.StartInfo.Arguments = arguments;

            process.Start();
            process.WaitForExit();

            int exitCode = process.ExitCode;

            if (exitCode != 0)
                throw new Exception($"Процесс генерации завершился с ошибкой.");

        }
    }
}
