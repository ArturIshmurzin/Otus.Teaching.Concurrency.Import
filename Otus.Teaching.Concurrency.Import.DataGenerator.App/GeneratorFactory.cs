using Otus.Teaching.Concurrency.Import.Core.Generators;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IFileGenerator GetGenerator(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".xml":
                    return new Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator(new RandomCustomerGenerator());
                case ".csv":
                    return new CSVGenerator(new RandomCustomerGenerator());
                default:
                    throw new System.Exception($"������ \"{fileExtension}\" �� �������������� ��� ���������.");
            }

        }
    }
}